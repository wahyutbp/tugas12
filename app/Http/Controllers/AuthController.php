<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('/form');
    }


    public function welcome(Request $request){
        $namadepan =$request['firstname'];
        $namabelakang =$request['lastname'];
        $jk=$request['gender'];
        $warganegara =$request['nationality'];
        $bahasa =$request['language'];
        $bio=$request['bio'];

        return view ('welcome',['namadepan'=>$namadepan,'namabelakang'=>$namabelakang,
        'jk'=>$jk,'warganegara'=>$warganegara,'bahasa'=>$bahasa,
        'bio'=>$bio
    ]);

    }
}
