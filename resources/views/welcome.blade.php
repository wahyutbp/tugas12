<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>welcome</title>
</head>
<body>
    
    <h1>SELAMAT DATANG! {{$namadepan}} {{$namabelakang}}</h1> <br>
    <h2>Nama depan anda : {{$namadepan}}</h2>
    <h2>Nama belakang anda : {{$namabelakang}}</h2>
    <h2>Jenis Kelamin anda adalah : 
        @if($jk==="1")
        Male
        @elseif ($jk==="2")
        Female
        @else 
        Other
        @endif
    </h2>
    <h2>Anda berkewarganegaraan : {{$warganegara}}</h2>
    <h2>Anda berbahasa : {{$bahasa}}</h2>
    <h2>Bio anda : {{$bio}}</h2>
    
</body>
</html>