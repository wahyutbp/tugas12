@extends('layout.master')
    
@section('judul')
   Halaman Form
@endsection

@section('content')
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="post">
    @csrf
    <label>First Name :</label>                         <br>
    <input type="text" name="firstname" placeholder="Wahyu Tri">         <br><br>

    <label>Last Name :</label>                          <br>
    <input type="text" name="lastname" placeholder="Budi Pangestu">     <br><br>

    <label>Gender :</label>                             <br>
    <input type="radio" value ="1" name="gender">Male    <br>
    <input type="radio" value="2"name="gender">Female  <br>
    <input type="radio" value="3" name="gender">Other   <br><br>

    <label>Nationality :</label>
    <select name="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Rusian">Rusian</option>
    </select>                                                 <br><br>

    <label>Language Spoken :</label>                          <br>
    <input type="checkbox" value ="Bahasa Indonesia" name="language">Bahasa Indonesian  <br>
    <input type="checkbox" value="English" name="language">English            <br>
    <input type="checkbox" value ="Other" name="language">Other              <br><br>

    <label>Bio :</label>                                      <br>
    <textarea cols="15" rows="8" placeholder="Moto Hidup Saya adalah terus berkarya!" name="bio"></textarea><br><br>
    <input type="submit" value="Kirim">


    </form>
@endsection